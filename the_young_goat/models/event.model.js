const mongoose = require('mongoose')
const Schema = mongoose.Schema

var evtSchema = new Schema({
  title: { type: String, required: true },
  _studentID: [{ type: Schema.Types.ObjectId, ref: 'Student' }],
  _subjectID: { type: Schema.Types.ObjectId, ref: 'Subject' },
  start: { type: String, required: true },
  end: { type: String, required: true }
})

const Event = mongoose.model('Event', evtSchema)

module.exports = Event
