const mongoose = require('mongoose')
const Schema = mongoose.Schema

var stdSchema = new Schema({
  StudentID: { type: String, required: true },
  Password: { type: String, required: true },
  Name: { type: String, required: true },
  Surname: { type: String, required: true },
  Year: { type: Number, required: true },
  Speciality: { type: String, required: true },
  Mail: { type: String, required: true }
})

const Student = mongoose.model('Student', stdSchema)

module.exports = Student
