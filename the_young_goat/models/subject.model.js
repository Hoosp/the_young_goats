const mongoose = require('mongoose')
const Schema = mongoose.Schema

var subSchema = new Schema({
  SubjectID: { type: String, required: true },
  Name: { type: String, required: true },
  Professor: { type: String, required: true }
})

const Subject = mongoose.model('Subject', subSchema)

module.exports = Subject
