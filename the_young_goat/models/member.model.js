const mongoose = require('mongoose')
const Schema = mongoose.Schema

var mbrSchema = new Schema({
  StudentID: { type: Schema.Types.ObjectId, ref: 'Student' },
  AssociationID: { type: Schema.Types.ObjectId, ref: 'Association' },
  Rank: { type: String, required: true }
})

const Member = mongoose.model('Member', mbrSchema)

module.exports = Member
