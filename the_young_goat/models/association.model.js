const mongoose = require('mongoose')
const Schema = mongoose.Schema

var assocSchema = new Schema({
  AssociationID: { type: String, required: true },
  Name: { type: String, required: true }
})

const Association = mongoose.model('Association', assocSchema)

module.exports = Association
