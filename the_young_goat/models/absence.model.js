const mongoose = require('mongoose')
const Schema = mongoose.Schema

var absSchema = new Schema({
  AbsenceID: { type: String, required: true },
  StudentID: { type: Schema.Types.ObjectId, ref: 'Student' },
  SubjectID: { type: Schema.Types.ObjectId, ref: 'Subject' }
})

const Absence = mongoose.model('Absence', absSchema)

module.exports = Absence
