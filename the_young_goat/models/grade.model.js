const mongoose = require('mongoose')
const Schema = mongoose.Schema

var grdSchema = new Schema({
  StudentID: { type: Schema.Types.String, ref: 'Student' },
  SubjectID: { type: Schema.Types.String, ref: 'Subject' },
  Name: { type: String, required: true },
  Value: { type: Number, required: true },
  Coeff: { type: Number, required: true }
})

const Grade = mongoose.model('Grade', grdSchema)

module.exports = Grade
