const express = require('express')
const session = require('express-session')
const mongoose = require('mongoose')
const path = require('path')
const bodyParser = require('body-parser') // pour parser les requêtes POST

mongoose.connect('mongodb://localhost:27017/Project_YG')

const student = require('./routes/student.js')
//  const article = require('./routes/article.js')

var app = express()

function onlyIfLoggedMiddleware (req, res, next) {
  if (req.user) {
    next()
  } else {
    res.redirect('/student/login')
  }
}

app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: false })) // for simple form posts
app.use(bodyParser.json()) // for API requests
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(session({
  secret: 'mysecret',
  name: 'sessId',
  userID: undefined,
  username: undefined,
  password: undefined
}))

app.use('/the_young_goat/student', student.router)
app.use(student.tokenToUserMiddleware)
app.use('/the_young_goat/student/grades', onlyIfLoggedMiddleware)

app.get('/the_young_goat', (req, res) => {
  res.send('ok')
})

app.listen(3000, () => {
  console.log('Application démarrée sur le port 3000!')
})
