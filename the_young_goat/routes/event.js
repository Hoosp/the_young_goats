const express = require('express')
const router = express.Router()
const fs = require('fs')

// chargement du modèle User
const Event = require('../models/event.model.js')
const Student = require('../models/student.model.js')
const Subject = require('../models/subject.model.js')

const studentEvent = []

var obj = {
  table: []
};

async function readEvents () {
  var events = Event.find({}, function (err, allEvents) {
    if (err) throw err
    // events = JSON.parse(allEvents)
    // console.log(allEvents)
  })
  return events
}

readEvents()

router.get('/getAllEventByStudentId', async (req, res) => {
  try {
    const student = await Student.findOne({ _id: req.session.userID })
    if (!student) {
      console.log('No student found')
      return
    }
    const events = await Event.find({})
    events.forEach(element => {
      element._studentID.forEach(ele => {
        if (ele.toString() === student._id.toString()) {
          studentEvent.push(element)
        } else {
          console.log('Nothing here')
        }
      })
    })
    console.log('recherche effectuée!')
    console.log(studentEvent)

    studentEvent.forEach(element => {
      obj.table.push({ title: element.title, start: element.start, end: element.end })
    })
    var json = JSON.stringify(obj, null, 2)
    fs.writeFile('myjsonfile.json', json, (err) =>{
      if (err) throw err;
      console.log('data written to json file')
    })
    res.redirect('/student/home')
  } catch (err) {
    console.log(err)
    res.status(403).send(err)
  }
})

router.get('/', async (req, res) => {
  const student = await Student.findOne({ username: '123' })
  // console.log('student:' + student)
  const subject = await Subject.findOne({ subjectID: '1' })
  // console.log('subject:' + subject)
  console.log('aaaaaaaaaaaaaaaa')

  var studentArray = new Array()
  studentArray.push(student._id)

  if (student && subject) {
    try {
      const event = new Event({
        title: subject.title,
        _studentID: studentArray,
        _subjectID: subject._id,
        start: '2020-22-01T12:00:00',
        end: '2020-22-01T14:00:00'
      })
      await event.save()
      res.send('Event enregistré')
    } catch (err) {
      console.log('error: ' + err)
      res.status(403).send(err)
    }
  } else {
    console.log('Il y a un truc qui cloque')
    res.send('fail')
  }
})


module.exports = router
