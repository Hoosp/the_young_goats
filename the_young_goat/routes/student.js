const express = require('express')
const alert = require('alert-node')
const bcrypt = require('bcryptjs')

// chargement du modèle Student
const router = express.Router()
const Student = require('../models/student.model.js')
const Grade = require('../models/grade.model.js')

async function tokenToUserMiddleware (req, res, next) {
  if (req.session && req.session.userID) {
    req.user = await Student.findById(req.session.userID)
  }
  next()
}

router.use(tokenToUserMiddleware)

router.get('/login', (req, res) => {
  res.render('login_post')
})

router.post('/login', async (req, res) => {
  const { StudentID, Password } = req.body
  // alert(StudentID)
  // alert(Password)

  try {
    const student = await Student.findOne({ StudentID: StudentID })

    if (student == null) {
      alert('Etudiant inconnu!')
      res.redirect('/the_young_goat/student/login')
    } else {
      const result = await bcrypt.compare(Password, student.Password)
      // const studentpassword = await Student.findOne({ StudentID: StudentID, Password: Password })

      if (result === null) {
        // res.send('Mot de passe incorrect')
        alert('Mot de passe incorrect')
        res.redirect('/the_young_goat/student/login')
      }
    }

    req.session.studentID = student.StudentID
    req.session.password = student.Password
    req.session.userID = student._id

    console.log(req.session.studentID)
    console.log(req.session.password)
    console.log(req.session.userID)
    res.redirect('/the_young_goat/student/grade')
  } catch (err) {
    console.log(err)
    res.status(403).send(err)
  }
})

router.get('/grade', async (req, res) => {
  if (req.user) {
    const notes = await Grade.find({ StudentID: req.session.studentID })
    const subs = []
    const notation = []
    const coefficient = []
    const moy = []
    let cpt = 0
    subs.push(notes[0].SubjectID) // get subjects of student's grades
    notes.forEach(elem => {
      if (subs.includes(elem.SubjectID)) {
        coefficient.push(elem.Coeff)
        notation.push(elem.Value)
      } else {
        subs.push(elem.SubjectID)
        coefficient.push(elem.Coeff)
        notation.push(elem.Value)
      }
      cpt += elem.Coeff * elem.Value
      moy.push(cpt)
      cpt = 0
    })

    moy.forEach(elem => {
      console.log(elem)
    })

    res.render('grades', {
      stu: req.session.studentID,
      subs: subs,
      notation: notation,
      coeff: coefficient,
      moy: moy
    })
  } else {
    res.redirect('/the_young_goat/student/login')
  }
})

router.post('/register', async (req, res) => {
  const { StudentID, Password, Name, Surname, Year, Speciality, Mail } = req.body
  const student = await Student.findOne({ StudentID: StudentID })
  if (!student) {
    try {
      const hash = await bcrypt.hash(Password, 8)
      const newStudent = new Student({
        StudentID: StudentID,
        Password: hash,
        Name: Name,
        Surname: Surname,
        Year: Year,
        Speciality: Speciality,
        Mail: Mail
      })
      await newStudent.save()
      res.send('Etudiant enregistré !')
    } catch (err) {
      console.log(err)
      // res.status(403).send('Wrong :  this studient already exists')
      res.status(403).send(err)
    }
  } else {
    console.log('Etudiant déjà existant')
    res.render('login_post')
  }
})

router.get('/findStudent', async (req, res) => {
  const result = await Student.findOne({ username: '123' })
  console.log(result.username)
  res.send(result.username)
})

router.get('/home', async (req, res) => {
  if (req.user) {
    res.render('home')
  } else {
    res.redirect('/the_young_goat/student/login')
  }
})

router.get('/assoc', async (req, res) => {
  if (req.user) {
    res.render('assoc')
  } else {
    res.redirect('/the_young_goat/student/login')
  }
})

router.get('/abs', async (req, res) => {
  if (req.user) {
    res.render('abs')
  } else {
    res.redirect('/the_young_goat/student/login')
  }
})

router.get('/index', async (req, res) => {
  if (req.user) {
    res.render('index', {
      student: req.session.StudentID
    })
  } else {
    res.redirect('/the_young_goat/student/login')
  }
})

router.get('/logout', (req, res) => {
  req.session.destroy()
  if (req.user) {
    res.send('Au revoir ' + req.user.firstname + ' ! ')
  } else {
    res.send('Mdr t"es qui ?')
  }
})

module.exports = {
  router,
  tokenToUserMiddleware
}
