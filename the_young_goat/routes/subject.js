const express = require('express')
const router = express.Router()

// chargement du modèle User
const Subject = require('../models/subject.model.js')

router.get('/', (req, res) => {
  res.render('subject')
})

router.post('/subject', (req, res) => {
  try {
    const newSubject = new Subject({
      subjectID: '1',
      name: 'NodeJS',
      professor: 'CHERREL'
    })
    newSubject.save()
    res.send('Nouvelle matière créee')
  } catch (err) {
    console.log(err)
    res.status(403).send(err)
  }
})

router.get('/findsubject', async (req, res) => {
  const result = await Subject.findOne({ subjectID: '1' })
  console.log(result)
  res.send(result)
})

// coding here

module.exports = router
