# The_Young_Goats

**Analyse :**
Le sujet de ce projet s’oriente autour de la création d’un site internet permettant la gestion en ligne de l’espace d’un étudiant d’une école, à l’instar de MyEfrei.
L’étudiant, par le biai de ce site, pourra accéder aux informations classique  permettant de gérer sa vie étudiante dans son école.

Le site web se dotera, avant toute consultation d’informations, d’un système d’authentification avec nom et mot de passe du comptes de l’étudiant. Il n’y aura pas de possibilité de créer un compte.

Après authentification de l’étudiant, le site permettra l’affichage des informations importantes concernant ce dernier. Les informations mises à disposition de l’étudiant seront les suivantes :
* Emploi du temps sur la semaine.
* Notes obtenues par matière et coefficient.
* Informations concernant l’école.
* Statut des absences.
* Informations concernant la ou les éventuelles associations suivies.

En parallèle, le site permettra à l’étudiant de souscrire à des associations et gérer leurs créneaux (ajouter ou retirer un créneau par exemple).
L’affichage des informations sera sous la forme d’un dashboard afin de regrouper toutes les informations nécessaires et importante pour l’étudiant.


**Conception :**
Il faudra créer une base de données qui contiendra les tables suivantes :
* Etudiants : id de l’étudiant, mot de passe, informations personnelles (nom, prénom, etc…) et clés étrangères vers d’autres tables;
* Evenement : id de l’événement, id de la matière/association, date et durée, liste des participants (Etudiants);
* Matières : id de la matière, nom de la matière, nom du professeur;
* Notes : id de la matière, intitulé de la note, id de l’étudiant, valeur, coefficient, date, commentaire;
* Absences : id de l'événement, id de l’étudiant;
* Associations : id de l’association, nom de l’assoc, membres du bureau (Etudiants), liste des membres (Etudiants).

Il y aura 6 pages :
* Page d’identification avec identifiant et mot de passe.
* Page d’accueil de l’étudiant sous la forme d’un dashboard.
* Liste des notes par semestre et par matière.
* Liste des associations dont l’étudiant est membre avec leurs infos (coordonnées du bureau, etc…).
* Liste des absences par semestre.
* Emploi du temps par semaine.


**Planification :**
Au vu de notre niveau en nodeJS, nous avons estimé que la durée le projet correspond à la deadline finale. Nous commencerons par créer la base de données pour avoir une structure ordonnée et claire, puis nous réaliserons la base du site pour connecter la base de données, enfin, nous nous occuperons de l’aspect frontend afin de ne pas se retrouver avec un squelette sans chair.
Nous avons estimé le volume du travail pour chaque tâche ainsi que la deadline :
* Base de données : 2 semaines, 30 Novembre.
* Pages du site : 7 semaines.
* Backend : 4 semaines, 16 Décembre.
* Frontend : 3 semaines, 1 Janvier.
* Finition : 2 semaines, 10 Janvier.


Nous prévoyons de faire des réunions chaque Vendredi pour faire le point :
* Ce qu’on a fait.
* Ce qui pose problème.
* Ce qui reste à faire.
* Ce que l’on va faire pour la semaine prochaine.

**Prototype :**

* Voici les différentes fonctionnalités disponibles par page : 

*Page de connexion:*

* Connexion de l’utilisateur avec son identifiant et son mot de passe. 

*Accueil :*
* Page web affichant les liens vers les différentes fonctionnalités de l’espace étudiant (déconnection, emploi du temps etc.);
* Informations générales à propos de l’étudiant.


*Affichage de l’emploi du temps :*

* L’étudiant peut voir son emploi du temps sous forme d’un calendrier (semaine) avec ses différents créneaux (cours ou associatifs). 


*Notes :*

* Sous forme de liste avec le nom de la matière et la note associée , l’étudiant peut consulter l’ensemble de ses notes avec les coefficients et le commentaire de l’évaluateur. 

*Affichage des absence:*
* Compteur du nombre d’absence total.
* id et nom de la matière, nom du professeur.

*Associations :*
* Page web.
* Coordonnées des membres du bureau (mail ou téléphone).


Ensuite le seul réel test de prototype à réaliser est celui de l’authentification (username, mot de passe) :
* L’utilisateur entre son nom et son mot de passe dans un formulaire sur une page web dédié à l’authentification et accède à la page d’accueil de son espace étudiant. 
* Si les paramètres entrés ne sont pas correctes, une erreur apparaît et l’utilisateur doit alors à nouveau entrer ses logs.
* Si l’utilisateur n’est pas connecté, impossibilité d’accès aux autres pages.
* Si l’utilisateur est connecté, adaptation des informations relatives à l’utilisateur connecté.

